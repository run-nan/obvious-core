# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.0] - 2020-08-03
### Added
- State and event communication capabilities
- Application scheduling capability
- Ability to access middleware

## [0.1.1] - 2020-08-04
### Fixed
- fix some bugs

## [0.2.1] - 2020-12-10
### Added
- enable to watch and set state deeply

## [0.2.2] - 2020-12-14
### Fixed
- fix the error in tsconfig.json
- add the new method existState on socket